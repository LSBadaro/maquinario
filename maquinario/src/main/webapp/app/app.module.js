(function() {
	'use strict';
	angular.module('app', 
			['ngResource',
			 'ngRoute',
			 'ui.growl',
			 // app modules
			 'app.maquina',
			 'app.status',
			 'app.param',
			 'app.dashboard'
			 ]);
})();