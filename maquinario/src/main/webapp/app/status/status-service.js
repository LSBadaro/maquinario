(function(){
	'use strict';
	angular.module('app.status.service', 
			['app']).factory('StatusService', StatusService);
	
	StatusService.$inject = ['$http', '$resource'];
	function StatusService($http, $resource) {
		return {
			search: function(){
				return $http.get('rest/status');
			},
			save: function(status){
				return $http.post('rest/status', status);
			},
			edit: function(status){
				return $http.put('rest/status', status);
			},
			deletar: function(codigo){
				return $http.delete('rest/status/delete/' + codigo);
			},  
			carregarEntidade: function(id) {
				return $http.get('rest/status/entidade/' + id);
			}
		};
	}
	
})();