(function() {
	'use strict';
	angular.module('app.status', ['app', 'app.status.service'])
			.controller('StatusIndexCtrl', StatusIndexCtrl)
			.controller('StatusEditCtrl', StatusEditCtrl);

	/**
	 * Index.
	 */
	function StatusIndexCtrl( $scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, StatusService) {

		var vmscope = $scope;
		var vm = this;
		
		vmscope.newTitle = "Status";
		vm.status = [];
		
		vmscope.novoStatus = function() {
			$window.location.href = '#!/status/0';
		}

		vmscope.edit = function(id) {
			$window.location.href = '#!/status/' + id;
		}
		
		vm.listarStatus = function() {
			StatusService.search().then(function(resp) {
				  vm.status = resp.data;
				  if(vm.status.length == 0) {
					  $growl.box('Status', 'Nenhum registro encontrado!', {
				            class: 'info',
				            sticky: false,
				            timeout: 5000
				        }).open();
				  }
			});
		}
		vm.listarStatus();
	}
	
	function StatusEditCtrl($scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, StatusService) {

		var vmscope = $scope;
		var vm = this;

		vmscope.newTitle = "Status";
		vmscope.isNew = true;

		if($routeParams.statusId != 0) {
			vmscope.isNew = false;
		}
		
		vmscope.entity = {
				codigo: null,
				nome:null
		}
		
		vmscope.save = function() {
			if(vmscope.entity.codigo != null && vmscope.entity.nome != null){
				StatusService.save(vmscope.entity).then(function(resp) {
					$window.location.href = '#!/status/' + resp.data.codigo;
					$growl.box('Status', 'Dados salvos com sucesso!', {
			            class: 'success',
			            sticky: false,
			            timeout: 5000
			        }).open();
				});
			} else {
				$growl.box('Status', 'Preencha todos os campos!', {
		            class: 'warning',
		            sticky: false,
		            timeout: 5000
		        }).open();
			}
		}
		
		vmscope.edit = function(Status) {
			StatusService.edit(vmscope.entity).then(function(resp) {
				vmscope.entity = resp.data;
				$growl.box('Status', 'Dados alterados com sucesso!', {
		            class: 'success',
		            sticky: false,
		            timeout: 5000
		        }).open();
			}).catch(function(e){
				$growl.box('Status', e.status + ' - Dados alterados por outro usuario!', {
		            class: 'danger',
		            sticky: false,
		            timeout: 5000
		        }).open();
			});;
		}
		
		vmscope.deletar = function() {
			StatusService.deletar($routeParams.statusId).then(function(resp) {
				vmscope.back();
			});
		} 
		
		vmscope.back = function() {
			$window.location.href = '#!/status';
		}
		
		vm.carregarEntidade = function() {
			if($routeParams.statusId > 0) {
				StatusService.carregarEntidade($routeParams.statusId).then(function(resp) {
					vmscope.entity = resp.data;
				});
			}
		}
		vm.carregarEntidade();
		
	}

})();