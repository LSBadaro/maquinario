<div class="dtStatusIndex">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="row">
					<div class="col-sm-12 conteudo-header">
						<span style="font-size: 30px">{{newTitle}}</span>
						<div class="btn-group btnGroup" role="group" aria-label="Basic example">
						  <button type="button" class="btn btn-outline-success" ng-click="novoStatus()">Novo</button>
						</div>
					</div>
				</div>
				<div class="conteudo-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-condensed">
						  <thead class="thead-dark">
						    <tr>
						      <th class="text-center" scope="col">C&oacute;digo do Status</th>
						      <th scope="col">Nome</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr ng-repeat="item in vm.status" ng-click="edit(item.codigo)">
						      <th class="text-center" scope="row">{{item.codigo}}</th>
						      <td>{{item.nome}}</td>
						    </tr>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
