(function(){
	'use strict';
	angular.module('app.maquina.service', 
			['app']).factory('MaquinaService', MaquinaService);
	
	MaquinaService.$inject = ['$http', '$resource'];
	function MaquinaService($http, $resource) {
		return {
			search: function(){
				return $http.get('rest/maquinas');
			},
			save: function(maquina){
				return $http.post('rest/maquinas', maquina);
			},
			edit: function(maquina){
				return $http.put('rest/maquinas', maquina);
			},
			deletar: function(id){
				return $http.delete('rest/maquinas/delete/' + id);
			},  
			carregarEntidade: function(id) {
				return $http.get('rest/maquinas/entidade/' + id);
			}
		};
	}
	
})();