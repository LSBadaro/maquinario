<div class="dtMaquinaIndex">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="row">
					<div class="col-sm-12 conteudo-header">
						<span style="font-size: 30px">{{newTitle}}</span>
						<div class="btn-group btnGroup" role="group" aria-label="Basic example">
						  <button type="button" class="btn btn-outline-success" ng-click="novaMaquina()">Novo</button>
						</div>
					</div>
				</div>
				<div class="conteudo-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-condensed">
						  <thead class="thead-dark"">
						    <tr>
						      <th class="text-center" scope="col">#</th>
						      <th scope="col">Nome do M&aacute;quina</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr ng-repeat="item in vm.maquinas" ng-click="edit(item.id)">
						      <th class="text-center" scope="row">{{item.id}}</th>
						      <td>{{item.nome}}</td>
						    </tr>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
