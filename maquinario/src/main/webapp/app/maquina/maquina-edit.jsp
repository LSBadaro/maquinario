<div class="dtMaquinaEdit">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="row">
					<div class="col-sm-12 conteudo-header">
						<span style="font-size: 30px">{{newTitle}}</span>
						<div class="btn-group btnGroup" role="group" aria-label="Basic example">
					  		<button type="button" class="btn btn-outline-success" ng-show="isNew" ng-click="save()">Salvar</button>
					  		<button type="button" class="btn btn-outline-success" ng-show="!isNew" ng-click="edit()">Editar</button>
					  		<button type="button" class="btn btn-outline-danger" ng-show="!isNew" ng-click="deletar()">Excluir</button>
						  	<button type="button" class="btn btn-outline-secondary" ng-click="back()">Voltar</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-success alert-dismissible fade show" role="alert" ng-model="vm.messsage" ng-show="vm.hasMessage">
						  {{message}}
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>
					</div>
				</div>
				<form class="conteudo-body">
				  <div class="form-row">
				    <div class="col-md-3 mb-3">
				      <label for="validationCustom01">Nome:</label>
				      <input type="text" class="form-control" id="validationCustom01" placeholder="Nome da sua maquina..." ng-model="entity.nome" required>
				    </div>
				  </div>
				</form>
			</div>
		</div>
	</div>
</div>