(function() {
	'use strict';
	angular.module('app.maquina', ['app', 'app.maquina.service'])
			.controller('MaquinaIndexCtrl', MaquinaIndexCtrl)
			.controller('MaquinaEditCtrl', MaquinaEditCtrl);

	/**
	 * Index.
	 */
	function MaquinaIndexCtrl( $scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, MaquinaService) {

		var vmscope = $scope;
		var vm = this;
		
		vmscope.newTitle = "Maquinas";
		vm.maquinas = [];
		
		vmscope.novaMaquina = function() {
			$window.location.href = '#!/maquina/0';
		}

		vmscope.edit = function(id) {
			$window.location.href = '#!/maquina/' + id;
		}
		
		vm.listarMaquinas = function() {
			MaquinaService.search().then(function(resp) {
				  vm.maquinas = resp.data;
				  if(vm.maquinas.length == 0) {
					  $growl.box('Maquina', 'Nenhum registro encontrado!', {
				            class: 'info',
				            sticky: false,
				            timeout: 5000
				        }).open();
				  }
			});
		}
		vm.listarMaquinas();
	}
	
	function MaquinaEditCtrl($scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, MaquinaService) {

		var vmscope = $scope;
		var vm = this;
		
		vmscope.isNew = true;
		vmscope.newTitle = "M\u00E1quina";
		vmscope.entity = {
				id: null,
				nome:null
		}
		
		if($routeParams.maquinaId != 0) {
			vmscope.isNew = false;
		}
		
		vmscope.save = function() {
			if(vmscope.entity.nome != null) {
				MaquinaService.save(vmscope.entity).then(function(resp) {
					$window.location.href = '#!/maquina/' + resp.data.id;
					$growl.box('M\u00E1quina', 'Dados salvos com sucesso!', {
			            class: 'success',
			            sticky: false,
			            timeout: 5000
			        }).open();
				});
			} else {
				$growl.box('M\u00E1quina', 'Preencha todos os campos!', {
		            class: 'warning',
		            sticky: false,
		            timeout: 5000
		        }).open();
			}
		}
		
		vmscope.edit = function() {
			MaquinaService.edit(vmscope.entity).then(function(resp) {
				vmscope.entity = resp.data;
				$growl.box('M\u00E1quina', 'Dados alterados com sucesso!', {
		            class: 'success',
		            sticky: false,
		            timeout: 5000
		        }).open();
			}).catch(function(e){
				$growl.box('M\u00E1quina', e.status + ' - Dados alterados por outro usuario!', {
		            class: 'danger',
		            sticky: false,
		            timeout: 5000
		        }).open();
			});
		}
		
		vmscope.deletar = function() {
			MaquinaService.deletar($routeParams.maquinaId).then(function(resp) {
				vmscope.back();
			});
		} 
		
		vmscope.back = function() {
			$window.location.href = '#!/maquina';
		}
		
		vm.carregarEntidade = function() {
			if($routeParams.maquinaId > 0) {
				MaquinaService.carregarEntidade($routeParams.maquinaId).then(function(resp) {
					vmscope.entity = resp.data;
				})
			}
		}
		vm.carregarEntidade();
		
	}

})();