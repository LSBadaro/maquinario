<div class="dtParamEdit">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="row">
					<div class="col-sm-12 conteudo-header">
						<span style="font-size: 30px">{{newTitle}}</span>
						<div class="btn-group btnGroup" role="group" aria-label="Basic example">
					  		<button type="button" class="btn btn-outline-success" ng-click="save()">Salvar</button>
						</div>
					</div>
				</div>
				<form class="conteudo-body">
				  <div class="form-row">
				  	<div class="col-md-3 mb-3">
				    	<label for="validationCustom01">Per&iacute;odo para troca de Status:</label>
				    	<div class="input-group">
					      	<input type="number" min="5" class="form-control" ng-model="entity.periodicidadeStatus" required>
					      	<div class="input-group-prepend">
								<div class="input-group-text">segundos</div>
							</div>
						</div>
				    </div>
				  </div>
				</form>
			</div>
		</div>
	</div>
</div>