(function(){
	'use strict';
	angular.module('app.param.service', 
			['app']).factory('ParamService', ParamService);
	
	ParamService.$inject = ['$http', '$resource'];
	function ParamService($http, $resource) {
		return {
			save: function(param){
				return $http.post('rest/param', param);
			},
			carregarEntidade: function(id) {
				return $http.get('rest/param');
			}
		};
	}
	
})();