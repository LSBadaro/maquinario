(function() {
	'use strict';
	angular.module('app.param', ['app', 'app.param.service'])
			.controller('ParamCtrl', ParamCtrl);

	function ParamCtrl($scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, ParamService) {

		var vmscope = $scope;
		var vm = this;

		vmscope.newTitle = "Per\u00EDodo";
		
		vmscope.entity = {
				periodicidadeStatus:null
		}
		
		vmscope.save = function() {
			ParamService.save(vmscope.entity).then(function(resp) {
				vmscope.entity = resp.data;
				$growl.box('Per\u00EDodo', 'Dados salvos com sucesso!', {
		            class: 'success',
		            sticky: false,
		            timeout: 5000
		        }).open();
			});
		}
		
		vmscope.back = function() {
			$window.location.href = '#!/param';
		}
		
		vm.carregarEntidade = function() {
			ParamService.carregarEntidade().then(function(resp) {
				if(resp.data != null) {
					vmscope.entity = resp.data;
				}
			});
		}
		vm.carregarEntidade();
		
	}

})();