(function(){
	'use strict';
	angular.module('app.dashboard.service', 
			['app']).factory('DashboardService', DashboardService);
	
	DashboardService.$inject = ['$http', '$resource'];
	function DashboardService($http, $resource) {
		return {
			carregarEventos: function() {
				return $http.get('rest/dashboard');
			}
		};
	}
	
})();