(function() {
	'use strict';
	angular.module('app.dashboard', ['app', 'app.dashboard.service'])
			.controller('DashboardCtrl', DashboardCtrl);

	function DashboardCtrl($scope, $interval, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, DashboardService) {

		var vmscope = $scope;
		var vm = this;

		vmscope.newTitle = "Eventos";
		vm.eventos = [];
		
		vmscope.sync = function() {
			DashboardService.carregarEventos().then(function(resp) {
				if(resp.data != null) {
					vm.eventos = resp.data;
				}
			});
		};
		
		vmscope.sync();
		
		$interval(vmscope.sync, 5000);
		
	}

})();