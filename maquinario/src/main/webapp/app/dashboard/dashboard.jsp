<div class="dtDashboard">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="row">
					<div class="col-sm-12 conteudo-header">
						<span style="font-size: 30px">{{newTitle}}</span>
						<div class="btn-group btnGroup" role="group" aria-label="Basic example">
					  		<button type="button" class="btn btn-outline-info" ng-click="sync()">Sync</button>
						</div>
					</div>
				</div>
				<div class="conteudo-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-condensed table-hover">
						  <thead class="thead-dark">
						    <tr>
						      <th class="text-left" scope="col">Nome da M&aacute;quina</th>
						      <th class="text-left" scope="col">C&oacute;digo do Status</th>
						      <th class="text-right" scope="col">C&oacute;digo do Status</th>
						      <th class="text-center" scope="col">Data</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr ng-repeat="item in vm.eventos">
						      <td class="text-left" scope="row">{{item.nomeMaquina}}</td>
						      <td class="text-left" scope="row">{{item.nomeStatus}}</td>
						      <td class="text-right" scope="row">{{item.codigoStatus}}</td>
						      <td class="text-center" scope="row">{{item.data | date:'dd/MM/yyyy'}}</td>
						    </tr>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
