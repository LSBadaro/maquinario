(function() {
	'use strict';
angular.
  module('app').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider
      	//Dashboard
      	.when('/dashboard', {templateUrl: 'app/dashboard/dashboard.jsp',
      		controller: 'DashboardCtrl', controllerAs: 'vm'
	    })
      
      	//Maquina
        .when('/maquina', {templateUrl: 'app/maquina/maquina-index.jsp',
        	controller: 'MaquinaIndexCtrl', controllerAs: 'vm'
        })
        .when('/maquina/:maquinaId', {templateUrl: 'app/maquina/maquina-edit.jsp',
        	controller: 'MaquinaEditCtrl', controllerAs: 'vm'
        })

        //Status
        .when('/status', {templateUrl: 'app/status/status-index.jsp',
        	controller: 'StatusIndexCtrl', controllerAs: 'vm'
        })
        .when('/status/:statusId', {templateUrl: 'app/status/status-edit.jsp',
        	controller: 'StatusEditCtrl', controllerAs: 'vm'
        })
        
        //Param
        .when('/param', {templateUrl: 'app/param/param-index.jsp',
        	controller: 'ParamCtrl', controllerAs: 'vm'
        })
        .otherwise('/dashboard');
    }
  ]);
})();