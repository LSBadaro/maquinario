<html ng-app="app">
	<head>
	  	<meta content='text/html; charset=UTF-8' http-equiv="Content-Type" />
		
	  	<link href="assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	  	<script src="assets/libs/jquery/2.1.1/jquery.min.js"></script>
		
		<!-- Latest compiled and minified JavaScript -->
	  	<link href="assets/libs/angular-growl/css/growl.css" rel="stylesheet">
	  	<link href="assets/css/style.css" rel="stylesheet">
	  	<script src="assets/libs/angularjs/angular.js"></script>
<!-- 	  	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
	  	<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
	  	<script src="assets/libs/angularjs/angular-cookies.min.js"></script>
	  	<script src="assets/libs/angularjs/i18n/angular-locale_pt-br.js"></script>
		<script src="assets/libs/angularjs/angular-sanitize.min.js"></script>
		<script src="assets/libs/angularjs/angular-animate.min.js"></script>
		<script src="assets/libs/angular-growl/js/growl.js"></script>
	  	<script src="assets/libs/angularjs/angular-resource.min.js"></script>
  		<script src="assets/libs/angularjs/angular-route.min.js"></script>
	</head>
	<body>
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-expand-lg navbar-light navbar-menu">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					    <span class="navbar-toggler-icon"></span>
				    </button>
			        <div class="collapse navbar-collapse" id="navbarSupportedContent">
					    <ul class="navbar-nav navbar-custom">
					      	<li class="nav-item">
					        	<a class="nav-link" href="#!/dashboard">Home</a>
					      	</li>
					      	<li class="nav-item">
					        	<a class="nav-link" href="#!/maquina">M&aacute;quina</a>
					      	</li>
					      	<li class="nav-item">
					        	<a class="nav-link" href="#!/status">Status</a>
					      	</li>
					      	<li class="nav-item">
					        	<a class="nav-link" href="#!/param">Per&iacute;odo</a>
				        	</li>
						</ul>
				    </div>
				</nav>
			</div>
		</div>
		<div class="form-center">
			<div class="row">
				<div class="col-12">
					<div ng-view class="container">
					</div>
				</div>
			</div>
		</div>
		
		<script src="app/app.module.js"></script>
  		<script src="app/app.config.js"></script>
  		<script src="app/dashboard/dashboard-service.js"></script>
  		<script src="app/dashboard/dashboard-controller.js"></script>
  		<script src="app/maquina/maquina-service.js"></script>
  		<script src="app/maquina/maquina-controller.js"></script>
  		<script src="app/param/param-service.js"></script>
  		<script src="app/param/param-controller.js"></script>
  		<script src="app/status/status-service.js"></script>
  		<script src="app/status/status-controller.js"></script>
	</body>
</html>