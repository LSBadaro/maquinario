package lucas.maquinario.maquina;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
	@NamedQuery(name="procurarMaquinas",
		query="select maquina "
		+ "from Maquina maquina"),
	@NamedQuery(name="recuperarEntidade",
		query="select maquina "
		+ "from Maquina maquina "
		+ "where maquina.id = :id"),
	@NamedQuery(name="procurarIdsMaquinas",
		query="select maquina.id " 
	    + "from Maquina maquina")})
public class Maquina {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO )
	private Long id;
	
	@NotNull
	private String nome;
	
	@Version
	private Integer version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
