package lucas.maquinario.maquina;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class MaquinaService {

	@PersistenceContext
	private EntityManager em;
	
	public List<Maquina> recuperarMaquinas() {
		return em.createNamedQuery("procurarMaquinas", Maquina.class)
				.getResultList();
	}

	public Maquina recuperarMaquina(Long id) {
		return em.createNamedQuery("recuperarEntidade", Maquina.class)
				.setParameter("id", id)
				.getResultList().get(0);
	}

}
