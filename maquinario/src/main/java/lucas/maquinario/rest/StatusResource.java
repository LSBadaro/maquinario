package lucas.maquinario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lucas.maquinario.status.Status;
import lucas.maquinario.status.StatusService;

@Path("/status")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class StatusResource extends CrudResource<Object> {
	
	@EJB
	private StatusService svc;
	
	@PersistenceContext
	private EntityManager em;
	
	@GET
	public Response search() throws Exception {
		
		List<Status> result = new ArrayList<>();
		
		try {
			result = svc.recuperarStatus();
		} catch (Exception e) {
			//noop
		}
		
		return Response.ok(result).build();
	}
	
	@GET
	@Path("/entidade/{codigo}")
	public Status recuperarStatus(@PathParam("codigo") final Long codigo) {
		if(codigo != null) {
			Status status = svc.recuperarStatus(codigo);
			return status;
		}
		return null;
	}
	
	@POST
	public Status saveStatus(Status status) {
		if(status != null) {
			save(status);
		}
		return status;
	}
	
	@PUT
	public Status editStatus(Status status) {
		if(status != null) {
			update(status);
		}
		return status;
	}
	
	@DELETE
	@Path("/delete/{codigo}")
	public void deleteStatus(@PathParam("codigo") final Long codigo) {
		if(codigo != null) {
			Status status = em.find(Status.class, codigo);
			delete(status);
		}
	}

}
