package lucas.maquinario.rest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lucas.maquinario.param.Param;
import lucas.maquinario.status.EventoStatus;

@Singleton
@Startup
public class GeradorEventos {

	@PersistenceContext
	private EntityManager em;
	
	@Resource
    private TimerService timerService;
	
	private Timer timer; 
	
	@PostConstruct
	public void init() {
		Param param = em.find(Param.class, 1L);
		Long periodo = Long.valueOf(param.getPeriodicidadeStatus() * 1000);
		if(timer != null) {
			timer.cancel();
		}
		timer = timerService.createIntervalTimer(periodo, periodo, new TimerConfig());
	}
	
	@Timeout
	public void gerarEventos() {
		Random rnd = new Random();
		List<Long> maquinas = em.createNamedQuery("procurarIdsMaquinas", Long.class)
				.getResultList();
		
		List<Long> status = em.createNamedQuery("procurarIdsStatus", Long.class)
			.getResultList();
		
		if(maquinas.size() > 0) {
			maquinas.forEach(item -> {
				Integer statusIdx = rnd.nextInt(status.size());
				EventoStatus evento = new EventoStatus();
				
				evento.setCodigoStatus(status.get(statusIdx));
				evento.setIdMaquina(item);
				evento.setData(new Date());
				
				em.persist(evento);
			});
		}
		
	}
	
	/**
	 * Deleta dados de 5 minutos atras por causa que o banco usado e' em memoria.
	 */
	@Schedule(hour="*", minute="0/5", second="0")
	public void deletarDadosAntigos() {
		Date data = new Date(Instant.now().minus(5, ChronoUnit.MINUTES).toEpochMilli());
		em.createNamedQuery("deletarDadosVelhos")
			.setParameter("data", data)
			.executeUpdate();
	}
	
	
}
