package lucas.maquinario.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lucas.maquinario.status.DashboardService;
import lucas.maquinario.status.EventoStatus;

@Stateless
@Path("/dashboard")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DashboardResource extends CrudResource<Object> {
	
	@EJB
	private DashboardService svc;
	
	@GET
	public List<EventoStatus> recuperarEventos() {
		List<EventoStatus> evento = svc.recuperarEventos();
		return evento;
	}

}
