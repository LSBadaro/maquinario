package lucas.maquinario.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value="rest")
public class MaquinarioApplication extends Application {
	
}
