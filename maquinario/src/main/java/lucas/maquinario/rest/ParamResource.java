package lucas.maquinario.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lucas.maquinario.param.Param;
import lucas.maquinario.param.ParamService;

@Path("/param")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class ParamResource extends CrudResource<Object> {
	
	@EJB
	private ParamService svc;
	
	@EJB
	private GeradorEventos gerador;
	
	@GET
	public Param recuperarParam() {
		Param param = svc.recuperarParam();
		return param;
	}
	
	@POST
	public Param saveParam(Param param) {
		if(param != null) {
			update(param);
			gerador.init();
		}
		return param;
	}

}
