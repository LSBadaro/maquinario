package lucas.maquinario.rest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public abstract class CrudResource<E> {

	@PersistenceContext
	private EntityManager em;
	
	public Object save(Object entity) {
		em.persist(entity);
		return entity;
	}
	
	public Object update(Object entity) {
		try {
			em.merge(entity);
		} catch (Exception e) {
			throw new WebApplicationException(Response.status(Status.CONFLICT).build());
		}
		return entity;
	}
	
	public void delete(Object entity) {
		em.remove(entity);
	}
}
