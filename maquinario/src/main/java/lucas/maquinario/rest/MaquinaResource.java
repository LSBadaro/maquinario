package lucas.maquinario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lucas.maquinario.maquina.Maquina;
import lucas.maquinario.maquina.MaquinaService;

@Path("/maquinas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class MaquinaResource extends CrudResource<Object> {
	
	@EJB
	private MaquinaService svc;
	
	@PersistenceContext
	private EntityManager em;
	
	@GET
	public Response search() throws Exception {
		
		List<Maquina> result = new ArrayList<>();
		
		try {
			result = svc.recuperarMaquinas();
		} catch (Exception e) {
			//noop
		}
		
		return Response.ok(result).build();
	}
	
	@GET
	@Path("/entidade/{id}")
	public Maquina recuperarMaquina(@PathParam("id") final Long id) {
		if(id != null) {
			Maquina maquina = svc.recuperarMaquina(id);
			return maquina;
		}
		return null;
	}
	
	@POST
	public Maquina saveMaquina(Maquina maquina) {
		if(maquina != null) {
			save(maquina);
		}
		
		return maquina;
	}
	
	@PUT
	public Maquina editMaquina(Maquina maquina) {
		if(maquina != null) {
			update(maquina);
		}
		
		return maquina;
	}
	
	@DELETE
	@Path("/delete/{id}")
	public void deleteMaquina(@PathParam("id") final Long id) {
		if(id != null) {
			Maquina maquina = em.find(Maquina.class, id);
			delete(maquina);
		}
	}

}
