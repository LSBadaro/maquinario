package lucas.maquinario.param;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
public class Param {
	
	@Id
	private Long id;
	
	@NotNull
	private Integer periodicidadeStatus;
	
	@Version
	private Integer version;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPeriodicidadeStatus() {
		return periodicidadeStatus;
	}

	public void setPeriodicidadeStatus(Integer periodicidadeStatus) {
		this.periodicidadeStatus = periodicidadeStatus;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
