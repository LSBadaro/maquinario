package lucas.maquinario.param;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ParamService {
	
	@PersistenceContext
	private EntityManager em;
	
	public Param recuperarParam() {
		return em.find(Param.class, 1L);
	}

}
