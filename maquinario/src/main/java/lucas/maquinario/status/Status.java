package lucas.maquinario.status;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
	@NamedQuery(name="procurarStatus",
		query="select status "
		+ "from Status status"),
	@NamedQuery(name="recuperarEntidadeStatus",
		query="select status "
		+ "from Status status "
		+ "where status.codigo = :codigo"),
	@NamedQuery(name="procurarIdsStatus",
		query="select status.codigo " 
		+ "from Status status")})
public class Status {

	@Id
	private Long codigo;
	
	@NotNull
	private String nome;
	
	@Version
	private Integer version;
	
	public Long getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
