package lucas.maquinario.status;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class StatusService {

	@PersistenceContext
	private EntityManager em;
	
	public List<Status> recuperarStatus() {
//		Status status = new Status();
//		status.setCodigo(258L);
//		status.setNome("Codigo 258");
//		em.persist(status);
		
		return em.createNamedQuery("procurarStatus", Status.class)
				.getResultList();
	}

	public Status recuperarStatus(Long codigo) {
		return em.createNamedQuery("recuperarEntidadeStatus", Status.class)
				.setParameter("codigo", codigo)
				.getResultList().get(0);
	}

}
