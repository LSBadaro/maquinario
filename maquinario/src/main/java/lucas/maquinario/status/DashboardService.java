package lucas.maquinario.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DashboardService {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<EventoStatus> recuperarEventos() {
		
		List<?> eventos = em.createNamedQuery("recuperarEventos")
				.getResultList();
		
		List<EventoStatus> eventoStatus = new ArrayList<>();
		
		if(eventos.size() > 0) {
			eventos.forEach(e -> {
				Object[] linha = (Object[]) e;
				EventoStatus evento = new EventoStatus();
				evento.setData((Date) linha[3]);
				evento.setNomeMaquina((String) linha[0]);
				evento.setNomeStatus((String) linha[1]);
				evento.setCodigoStatus(((Number) linha[2]).longValue());
				eventoStatus.add(evento);
			});
		}
		
		return eventoStatus;
	}

}
