package lucas.maquinario.status;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@NamedNativeQuery(
		name="recuperarEventos",
		query = "select maq.nome maquinaNome, status.nome statusNome, status.codigo, evento.data from Maquina maq " + 
				"	left join EventoStatus evento on evento.idMaquina = maq.id" + 
				"	left join Status status on status.codigo = evento.codigoStatus" + 
				"	where evento.id = (" + 
				"			select id from EventoStatus " + 
				"				where idMaquina = maq.id " + 
				"				order by data desc " + 
				"				limit 1)")
@NamedQuery(
		name="deletarDadosVelhos",
		query="delete from EventoStatus"
				+ " where data < :data")
public class EventoStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private Long codigoStatus;
	
	@NotNull
	private Date data;
	
	@NotNull
	private Long idMaquina;
	
	@Version
	private Integer version;
	
	@Transient
	private String nomeMaquina;
	
	@Transient
	private String nomeStatus;
	
	public EventoStatus() {
		//noop
	}
	
	public EventoStatus(final String nomeMaquina,
						final String nomeStatus,
						final Date dataEvento) {
		super();
		this.nomeMaquina = nomeMaquina;
		this.nomeStatus = nomeStatus;
		this.data = dataEvento;
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigoStatus() {
		return codigoStatus;
	}

	public void setCodigoStatus(Long codigoStatus) {
		this.codigoStatus = codigoStatus;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getIdMaquina() {
		return idMaquina;
	}

	public void setIdMaquina(Long idMaquina) {
		this.idMaquina = idMaquina;
	}

	public String getNomeMaquina() {
		return nomeMaquina;
	}

	public void setNomeMaquina(String nomeMaquina) {
		this.nomeMaquina = nomeMaquina;
	}

	public String getNomeStatus() {
		return nomeStatus;
	}

	public void setNomeStatus(String nomeStatus) {
		this.nomeStatus = nomeStatus;
	}
	
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}