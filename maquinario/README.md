Observação
==

	# Deleta dados de 5 minutos atrás por causa que o banco usado é em memória

Requisitos para rodar a aplicacao
==

1. Java 8
2. Servidor WildFly 11

Como fazer o build da aplicacao
==

1. Via terminal, acesse a pasta do projeto
2. Execute o comando abaixo para fazer o build da app:
 
	# Linux
	./gradlew clean build
		
	# Windows
	gradlew.bat clean build
	
3. Copie o arquivo "<projeto>/build/libs/maquinario.war" para a pasta "<wildfly>/standalone/deployments"
4. Acesse a pasta "<wildfly>/bin" e execute o comando para subir o servidor:
	
	# Linux
	./standalone.sh
	
	# Windows
	standalone.bat
	
Como acessar a aplicacao
==
No browser, acesse o endereco "http://localhost:8080/maquinario"
